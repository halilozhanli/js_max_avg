let nums = [].concat(...data.map(d => d.data)).map(s => s.rating);
let sum = nums.reduce((a,b) => (a + b), 0);
let avg = sum / nums.length; //
let max = Math.max(...nums);
console.log(max, avg);
